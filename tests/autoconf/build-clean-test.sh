#!/bin/sh
git_clean_ndx () { 
    git clean -ndx
    }
git_clean_files_nr () { 
    git_clean_ndx | wc -l
    }
indent () { 
    cat | perl -pe 's/^/    /g'
    }

echo "#############################"
echo "### Testing make cleanups ###"
echo "#############################"

git_clean_files_nr_before_configure=$(git_clean_files_nr)
echo "Before './configure', there are ${git_clean_files_nr_before_configure} files to delete:"
git_clean_ndx | indent
if ! test "${git_clean_files_nr_before_configure}" -eq "0";then
    echo "There seem to be files to delete even before './configure'!?"
    exit 1 
fi

echo "Running './configure'..."
./configure

git_clean_files_nr_after_configure=$(git_clean_files_nr)
echo "After './configure', there are ${git_clean_files_nr_after_configure} files to delete:"
git_clean_ndx | indent
if ! test "${git_clean_files_nr_before_configure}" -lt "${git_clean_files_nr_after_configure}";then
    echo "'./configure' seems to have added no files!"
    exit 1 
fi

echo "### Running 'make'... ###"
make

git_clean_files_nr_after_make=$(git_clean_files_nr)
echo "After 'make', there are ${git_clean_files_nr_after_make} files to delete:"
git_clean_ndx | indent
if ! test "${git_clean_files_nr_after_configure}" -lt "${git_clean_files_nr_after_make}";then
    echo "'make' seems to have added no files!"
    exit 1 
fi

echo "### Running 'make clean' ###"
make clean

git_clean_files_nr_after_clean=$(git_clean_files_nr)
echo "After 'make clean', there are ${git_clean_files_nr_after_clean} files to delete:"
git_clean_ndx | indent
if ! test "${git_clean_files_nr_after_clean}" -lt "${git_clean_files_nr_after_make}";then
    echo "'make clean' obviously didn't delete any files!"
    exit 1 
fi
if ! test "${git_clean_files_nr_after_clean}" -eq "${git_clean_files_nr_after_configure}";then
    echo "'make clean' obviously didn't delete everything 'make' produced!"
    exit 1 
fi

echo "### Running 'make distclean' ###"
make distclean

git_clean_files_nr_after_distclean=$(git_clean_files_nr)
echo "After 'make distclean', there are ${git_clean_files_nr_after_distclean} files to delete:"
git_clean_ndx | indent
if ! test "${git_clean_files_nr_after_distclean}" -lt "${git_clean_files_nr_after_clean}";then
    echo "'make distclean' obviously didn't delete any files!"
    exit 1 
fi
if ! test "${git_clean_files_nr_after_distclean}" -eq "${git_clean_files_nr_before_configure}";then
    echo "'make distclean' obviously didn't reset everything to the 'before ./configure' state. It left ${git_clean_files_nr_after_distclean} files."
    exit 1 
fi

echo "'make clean' and 'make distclean' obviously do what they should!"
